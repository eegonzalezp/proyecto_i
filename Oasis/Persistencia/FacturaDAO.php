<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/ProductoDAO.php";
class FacturaDAO{
    private $idcliente;
    private $idfactura;
    private $nombre;
    private $cantidad;
    private $precio;
    
    public function getIdcliente(){
        return $this -> idProducto;
    }
    public function getIdfactura(){
        return $this -> idfactura;
    }
    
    public function getNombre(){
        return $this -> nombre;
    }
    
    public function getCantidad(){
        return $this -> cantidad;
    }
    
    public function getPrecio(){
        return $this -> precio;
    }
        
    public function FacturaDAO($idcliente = "", $idfactura = "", $nombre = "", $cantidad = "", $precio = ""){
        $this -> idcliente = $idcliente;
        $this -> idfactura = $idfactura;
        $this -> nombre = $nombre;
        $this -> cantidad = $cantidad;
        $this -> precio = $precio;
    }
       
    public function insertar(){
        return "insert into factura (idcliente, idfactura, Nombre, Cantidad, Precio )
                values ('" . $this -> idcliente . "', '" . $this -> idfactura . "', '" . $this -> nombre . "', '" . $this -> cantidad. "', '" . $this -> precio. "')";
    }
    
    public function consultarTodos(){
        return "select idcliente, idfactura, nombre, cantidad, precio 
                from factura Where idfactura = ".$this->idfactura ;
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select idcliente, idfactura, nombre, cantidad, precio
                from factura
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidad(){
        return "select count(idfactura)
                from factura";
    }
    
}

?>