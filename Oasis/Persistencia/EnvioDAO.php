<?php
class EnvioDAO
{
    private $idEnvio;
   
    private $ciudad;
    private $barrio;
    private $direccion;
    private $telefono;
    private $estado;

    public function getIdEnvio()
    {
        return $this->idEnvio;
    }

    public function getCiudad()
    {
        return $this->ciudad;
    }

    public function getBarrio()
    {
        return $this->barrio;
    }

    public function getDireccion()
    {
        return $this->direccion;
    }

    public function getTelefono()
    {
        return $this->telefono;
    }

    public function getEstado()
    {
        return $this->estado;
    }

    public function EnvioDAO($idEnvio = "", $idfactura = "", $ciudad = "", $barrio = "", $direccion = "", $telefono = "", $estado = "")
    {
        $this->idEnvio = $idEnvio;
        $this->idfactura = $idfactura;
        $this->ciudad = $ciudad;
        $this->barrio = $barrio;
        $this->direccion = $direccion;
        $this->telefono = $telefono;
        $this->estado = $estado;
    }
    public function insertarEnvio(){
        return "insert into envio (idfactura, ciudad, barrio, direccion, telefono, estado)
                values ('" . $this -> idfactura . "','" . $this -> ciudad . "', '" . $this -> barrio . "', '" . $this -> direccion . "', '" . $this -> telefono . "', '" . $this -> estado . "')";
    }
    
}
