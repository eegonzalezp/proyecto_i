<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/ProductoDAO.php";
class ProductoFDAO{
    private $idProducto;
    private $idfactura;
    private $nombre;
    private $cantidad;
    private $precio;
    
    public function getIdProducto(){
        return $this -> idProducto;
    }
    public function getIdfactura(){
        return $this -> idfactura;
    }
    
    public function getNombre(){
        return $this -> nombre;
    }
    
    public function getCantidad(){
        return $this -> cantidad;
    }
    
    public function getPrecio(){
        return $this -> precio;
    }
        
    public function ProductoFDAO($idProducto = "", $idfactura = "", $nombre = "", $cantidad = "", $precio = ""){
        $this -> idProducto = $idProducto;
        $this -> idfactura = $idfactura;
        $this -> nombre = $nombre;
        $this -> cantidad = $cantidad;
        $this -> precio = $precio;
    }
       
    public function insertar(){
        return "insert into Pedido ( Nombre, cantidad, precio, idfactura)
                values ('". $this -> nombre . "', '" . $this -> cantidad . "', '" . $this -> precio . "', '" . $this -> idfactura . "')";
    }
    
    public function consultarTodos(){
        return "select idProducto, nombre, cantidad, precio, idfactura 
                from Pedido Where idfactura = ".$this->idfactura ;
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select idProducto, nombre, cantidad, precio
                from Pedido
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidad(){
        return "select count(idProducto)
                from Pedido";
    }
    public function consultarCantidad2(){
        return"select count(nombre) 
                from Pedido where nombre = '".$this->nombre."'AND idfactura =".$this->idfactura ;
    }
    public function borrar(){
        return "delete from Pedido where idProducto ='".$this ->idProducto."'";

    }

    
}

?>