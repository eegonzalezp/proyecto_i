<?php

    class LogDao
    {
        private $id;
        private $accion;
        private $datos;
        private $fecha;
        private $hora;
        private $actor;
        
    
        public function getId()
        {
            return $this -> id;
        }

        public function getAccion()
        {
            return $this -> accion;
        }

        public function getDatos()
        {
            return $this -> datos;
        }

        public function getFecha()
        {
            return $this -> fecha;
        }

        public function getHora()
        {
            return $this -> hora;
        }

        public function getActor()
        {
            return $this -> actor;
        }

        public function LogDao($id = "", $accion = "", $datos = "", $fecha = "", $hora = "", $actor = ""){
            $this -> id = $id;
            $this -> accion = $accion;
            $this -> datos = $datos;
            $this -> fecha = $fecha;
            $this -> hora = $hora;
            $this -> actor = $actor;          
            
        }
        public function insertarLog(){
            return "insert into LogAdministrador (accion, datos, fecha, hora, actor)
                    values ('" . $this -> accion . "', '" . $this -> datos . "', " . $this -> fecha . ", " . $this -> hora . ", '" . $this -> actor . "')";
        }
        public function consultarFiltro($filtro){
            return "select Id, Accion, Datos, Fecha, Hora, Actor
                    from logadministrador
                    where Accion like '%" . $filtro . "%' or Datos like '" . $filtro . "%' or Fecha like '" . $filtro . "%' or Hora like '" . $filtro . "%' or Actor like '" . $filtro . "%'";
        }
    }
