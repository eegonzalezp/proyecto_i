-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-07-2020 a las 00:01:07
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdoasis`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `idAdministrador` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `clave` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `foto` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`idAdministrador`, `nombre`, `apellido`, `correo`, `clave`, `foto`) VALUES
(1, 'Steven', 'Gonzalez', 'steven@correo.com', '698d51a19d8a121ce581499d7b701668', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `idCliente` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `clave` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `foto` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` tinyint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`idCliente`, `nombre`, `apellido`, `correo`, `clave`, `foto`, `estado`) VALUES
(1, 'Camila', 'Mendez', 'camila@correo.com', 'c6f057b86584942e415435ffb1fa93d4', NULL, 1),
(2, 'Juan', 'Reyes', 'Juan@correo.com', 'c6f057b86584942e415435ffb1fa93d4', NULL, 1),
(3, 'Covid', '19', 'Covid@correo.com', 'c6f057b86584942e415435ffb1fa93d4', NULL, 1),
(4, 'sr x', 'gonzalez', 'sr@correo.com', 'c6f057b86584942e415435ffb1fa93d4', NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `envio`
--

CREATE TABLE `envio` (
  `idEnvio` int(25) NOT NULL,
  `idfactura` int(25) NOT NULL,
  `ciudad` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `barrio` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `estado` varchar(25) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `envio`
--

INSERT INTO `envio` (`idEnvio`, `idfactura`, `ciudad`, `barrio`, `direccion`, `telefono`, `estado`) VALUES
(2, 1594790492, 'Bogota', 'Rosales', 'Calle 68 bis sur', '7175924', 'Enviado'),
(3, 1594790638, 'Ibague', 'Altamira', 'trv 20 norte D', '3045432552', 'Enviado'),
(4, 1594842586, 'Medellin', 'Poblado', 'Calle 68 bis sur', '3045432552', 'Enviado'),
(5, 1595449805, 'Bogota', 'Bosa', 'Trv 49 #59 c 20 sur', '3045432552', 'Pendiente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `idcliente` int(11) NOT NULL,
  `idfactura` int(11) NOT NULL,
  `Nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `Cantidad` int(11) NOT NULL,
  `Precio` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`idcliente`, `idfactura`, `Nombre`, `Cantidad`, `Precio`) VALUES
(0, 1594790492, '', 0, '0'),
(0, 1594790638, '', 0, '0'),
(0, 1594842586, '', 0, '0'),
(0, 1595449805, '', 0, '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logadministrador`
--

CREATE TABLE `logadministrador` (
  `Id` int(11) NOT NULL,
  `Accion` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Datos` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `Fecha` date NOT NULL,
  `Hora` time NOT NULL,
  `Actor` varchar(200) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `logadministrador`
--

INSERT INTO `logadministrador` (`Id`, `Accion`, `Datos`, `Fecha`, `Hora`, `Actor`) VALUES
(1, 'Inició sesión', 'Steven Gonzalez', '2020-07-15', '00:12:45', 'Administrador'),
(2, 'Registró una pulpa', 'Steven Gonzalez', '2020-07-15', '00:13:38', 'Administrador'),
(3, 'Registró una pulpa', 'Steven Gonzalez', '2020-07-15', '00:13:56', 'Administrador'),
(4, 'Registró una pulpa', 'Steven Gonzalez', '2020-07-15', '00:14:13', 'Administrador'),
(5, 'Registró una pulpa', 'Steven Gonzalez', '2020-07-15', '00:14:30', 'Administrador'),
(6, 'Registró una pulpa', 'Steven Gonzalez', '2020-07-15', '00:14:48', 'Administrador'),
(7, 'Registró una pulpa', 'Steven Gonzalez', '2020-07-15', '00:15:07', 'Administrador'),
(8, 'Registró una pulpa', 'Steven Gonzalez', '2020-07-15', '00:15:23', 'Administrador'),
(9, 'Registró una pulpa', 'Steven Gonzalez', '2020-07-15', '00:15:43', 'Administrador'),
(10, 'Se registró', 'Juan Reyes', '2020-07-15', '00:18:34', 'Cliente'),
(11, 'Se registró', 'Covid 19', '2020-07-15', '00:19:29', 'Cliente'),
(12, 'Inició sesión', 'Camila Mendez', '2020-07-15', '00:21:14', 'Cliente'),
(13, 'Confirmó una compra', 'Camila Mendez', '2020-07-15', '00:22:24', 'Cliente'),
(14, 'Inició sesión', 'Juan Reyes', '2020-07-15', '00:23:47', 'Cliente'),
(15, 'Confirmó una compra', 'Juan Reyes', '2020-07-15', '00:24:43', 'Cliente'),
(16, 'Inició sesión', 'Camilo Mendoza', '2020-07-15', '00:25:09', 'Repartidor'),
(17, 'Envió un pedido', 'Camilo Mendoza', '2020-07-15', '00:25:10', 'Repartidor'),
(18, 'Inició sesión', 'Steven Gonzalez', '2020-07-15', '14:47:26', 'Administrador'),
(19, 'Registró una pulpa', 'Steven Gonzalez', '2020-07-15', '14:48:14', 'Administrador'),
(20, 'Editó una pulpa', 'Steven Gonzalez', '2020-07-15', '14:48:45', 'Administrador'),
(21, 'Inició sesión', 'Camila Mendez', '2020-07-15', '14:49:19', 'Cliente'),
(22, 'Confirmó una compra', 'Camila Mendez', '2020-07-15', '14:50:29', 'Cliente'),
(23, 'Inició sesión', 'Camilo Mendoza', '2020-07-15', '14:51:15', 'Repartidor'),
(24, 'Envió un pedido', 'Camilo Mendoza', '2020-07-15', '14:51:26', 'Repartidor'),
(25, 'Inició sesión', 'Steven Gonzalez', '2020-07-15', '14:52:24', 'Administrador'),
(26, 'Cambió el estado de un cliente', 'Steven Gonzalez', '2020-07-15', '14:52:49', 'Administrador'),
(27, 'Inició sesión', 'Covid 19', '2020-07-15', '14:53:28', 'Cliente'),
(28, 'Se registró', 'sr x gonzalez', '2020-07-15', '14:53:57', 'Cliente'),
(29, 'Se registró', 'Camila Mendez', '2020-07-15', '14:54:07', 'Cliente'),
(30, 'Inició sesión', 'Steven Gonzalez', '2020-07-22', '13:43:37', 'Administrador'),
(31, 'Cambió el estado de un cliente', 'Steven Gonzalez', '2020-07-22', '13:45:18', 'Administrador'),
(32, 'Registró una pulpa', 'Steven Gonzalez', '2020-07-22', '13:48:44', 'Administrador'),
(33, 'Registró una pulpa', 'Steven Gonzalez', '2020-07-22', '13:49:12', 'Administrador'),
(34, 'Registró una pulpa', 'Steven Gonzalez', '2020-07-22', '13:49:25', 'Administrador'),
(35, 'Registró una pulpa', 'Steven Gonzalez', '2020-07-22', '13:49:37', 'Administrador'),
(36, 'Registró una pulpa', 'Steven Gonzalez', '2020-07-22', '13:49:47', 'Administrador'),
(37, 'Registró una pulpa', 'Steven Gonzalez', '2020-07-22', '13:49:59', 'Administrador'),
(38, 'Registró una pulpa', 'Steven Gonzalez', '2020-07-22', '13:50:16', 'Administrador'),
(39, 'Registró una pulpa', 'Steven Gonzalez', '2020-07-22', '13:52:57', 'Administrador'),
(40, 'Inició sesión', 'Camila Mendez', '2020-07-22', '13:55:36', 'Cliente'),
(41, 'Inició sesión', 'Camilo Mendoza', '2020-07-22', '14:01:26', 'Repartidor'),
(42, 'Envió un pedido', 'Camilo Mendoza', '2020-07-22', '14:01:33', 'Repartidor'),
(43, 'Envió un pedido', 'Camilo Mendoza', '2020-07-22', '14:01:39', 'Repartidor'),
(44, 'Envió un pedido', 'Camilo Mendoza', '2020-07-22', '14:04:26', 'Repartidor'),
(45, 'Inició sesión', 'Camilo Mendoza', '2020-07-22', '15:17:26', 'Repartidor'),
(46, 'Envió un pedido', 'Camilo Mendoza', '2020-07-22', '15:17:37', 'Repartidor'),
(47, 'Inició sesión', 'Steven Gonzalez', '2020-07-22', '15:28:56', 'Administrador'),
(48, 'Cambió el estado de un cliente', 'Steven Gonzalez', '2020-07-22', '15:29:19', 'Administrador'),
(49, 'Registró una pulpa', 'Steven Gonzalez', '2020-07-22', '15:29:37', 'Administrador'),
(50, 'Inició sesión', 'Camila Mendez', '2020-07-22', '15:30:00', 'Cliente'),
(51, 'Confirmó una compra', 'Camila Mendez', '2020-07-22', '15:30:28', 'Cliente'),
(52, 'Inició sesión', 'Camilo Mendoza', '2020-07-22', '15:30:47', 'Repartidor'),
(53, 'Envió un pedido', 'Camilo Mendoza', '2020-07-22', '15:30:50', 'Repartidor'),
(54, 'Inició sesión', 'Camila Mendez', '2020-07-22', '15:33:07', 'Cliente'),
(55, 'Inició sesión', 'Steven Gonzalez', '2020-07-22', '16:48:07', 'Administrador'),
(56, 'Cambió el estado de un cliente', 'Steven Gonzalez', '2020-07-22', '16:48:10', 'Administrador'),
(57, 'Inició sesión', 'Camila Mendez', '2020-07-22', '16:48:48', 'Cliente'),
(58, 'Inició sesión', 'Camilo Mendoza', '2020-07-22', '16:49:19', 'Repartidor'),
(59, 'Envió un pedido', 'Camilo Mendoza', '2020-07-22', '16:49:23', 'Repartidor'),
(60, 'Inició sesión', 'Steven Gonzalez', '2020-07-22', '16:50:04', 'Administrador'),
(61, 'Registró una pulpa', 'Steven Gonzalez', '2020-07-22', '16:52:33', 'Administrador'),
(62, 'Inició sesión', 'Steven Gonzalez', '2020-07-22', '17:00:31', 'Administrador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido`
--

CREATE TABLE `pedido` (
  `idProducto` int(10) NOT NULL,
  `Nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Cantidad` int(10) NOT NULL,
  `Precio` int(45) NOT NULL,
  `idfactura` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `pedido`
--

INSERT INTO `pedido` (`idProducto`, `Nombre`, `Cantidad`, `Precio`, `idfactura`) VALUES
(4, 'Pulpa de mora', 2, 3500, 1594790492),
(8, 'Pulpa de uva', 2, 3500, 1594790492),
(9, 'Pulpa de fresa', 2, 3200, 1594790492),
(10, 'Pulpa de maracuya', 2, 4000, 1594790492),
(11, 'Pulpa de mango', 2, 3500, 1594790638),
(12, 'Pulpa de lulo', 2, 4000, 1594790638),
(13, 'Pulpa de guanabana', 2, 3500, 1594790638),
(14, 'Pulpa de guayaba', 2, 3200, 1594790638),
(16, 'Pulpa de uva', 5, 3500, 1594842586),
(17, 'Pulpa de uva', 1, 3200, 1595449805),
(19, 'Pulpa de maracuya', 1, 3500, 1595449805);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `idProducto` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` int(11) NOT NULL,
  `Descripcion` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `Foto` varchar(500) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`idProducto`, `nombre`, `cantidad`, `precio`, `Descripcion`, `Foto`) VALUES
(1, 'Pulpa de mora', 10, 3200, '', 'img/siempre/img_Pulpa de mora1595443724.jpg'),
(2, 'Pulpa de uva', 9, 3200, '', 'img/siempre/img_Pulpa de uva1595443752.jpg'),
(3, 'Pulpa de fresa', 10, 3500, '', 'img/siempre/img_Pulpa de fresa1595443765.jpg'),
(4, 'Pulpa de maracuya', 9, 3500, '', 'img/siempre/img_Pulpa de maracuya1595443777.jpg'),
(5, 'Pulpa de mango', 10, 3200, '', 'img/siempre/img_Pulpa de mango1595443787.jpg'),
(6, 'Pulpa de lulo', 10, 3500, '', 'img/siempre/img_Pulpa de lulo1595443799.jpg'),
(7, 'Pulpa de guanabana', 10, 3500, '', 'img/siempre/img_Pulpa de guanabana1595443816.jpg'),
(8, 'Pulpa de guayaba', 10, 3100, '', 'img/siempre/img_Pulpa de guayaba1595443977.jpg'),
(10, 'Pulpa de frutos rojos', 2, 3200, '', 'img/siempre/predeterminada.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `repartidor`
--

CREATE TABLE `repartidor` (
  `idRepartidor` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `clave` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `foto` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `repartidor`
--

INSERT INTO `repartidor` (`idRepartidor`, `nombre`, `apellido`, `correo`, `clave`, `foto`) VALUES
(1, 'Camilo', 'Mendoza', 'camilo@envios.com', 'bcbe3365e6ac95ea2c0343a2395834dd', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`idAdministrador`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idCliente`);

--
-- Indices de la tabla `envio`
--
ALTER TABLE `envio`
  ADD PRIMARY KEY (`idEnvio`),
  ADD KEY `idfactura` (`idfactura`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`idfactura`),
  ADD KEY `idfactura` (`idfactura`),
  ADD KEY `idfactura_2` (`idfactura`);

--
-- Indices de la tabla `logadministrador`
--
ALTER TABLE `logadministrador`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD PRIMARY KEY (`idProducto`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`idProducto`);

--
-- Indices de la tabla `repartidor`
--
ALTER TABLE `repartidor`
  ADD PRIMARY KEY (`idRepartidor`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `idAdministrador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idCliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `envio`
--
ALTER TABLE `envio`
  MODIFY `idEnvio` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `logadministrador`
--
ALTER TABLE `logadministrador`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT de la tabla `pedido`
--
ALTER TABLE `pedido`
  MODIFY `idProducto` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `idProducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `repartidor`
--
ALTER TABLE `repartidor`
  MODIFY `idRepartidor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `envio`
--
ALTER TABLE `envio`
  ADD CONSTRAINT `envio_ibfk_1` FOREIGN KEY (`idfactura`) REFERENCES `factura` (`idfactura`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
