<?php
class ProductoDAO{
    private $idProducto;
    private $nombre;
    private $cantidad;
    private $precio;
    private $Descripcion;
    private $Foto;
       
    public function ProductoDAO($idProducto = "", $nombre = "", $cantidad = "", $precio = "", $Descripcion="", $Foto=""){
        $this -> idProducto = $idProducto;
        $this -> nombre = $nombre;
        $this -> cantidad = $cantidad;
        $this -> precio = $precio;
        $this -> descripcion = $Descripcion;
        $this -> foto = $Foto;
    }
    public function consultar(){
        return "select nombre, cantidad, precio, foto
                from Producto
                where idProducto = '" . $this -> idProducto .  "'";
    }
       
    public function insertar(){
        return "insert into Producto (nombre, cantidad, precio, descripcion, foto)
                values ('" . $this -> nombre . "', '" . $this -> cantidad . "', '" . $this -> precio . "', '" .$this -> descripcion . "', '" . $this -> foto ."')";
    }
    
    public function consultarTodos(){
        return "select idProducto, nombre, cantidad, precio, descripcion, foto
                from Producto";
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select idProducto, nombre, cantidad, precio, descripcion, foto
                from Producto
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidad(){
        return "select count(idProducto)
                from Producto";
    }
    public function editar(){
        return "update Producto 
                SET cantidad =".$this ->cantidad. " where idProducto=".$this ->idProducto ;
    }
    public function consultar2(){
        return "select idProducto, nombre, cantidad, precio
                from Producto where nombre='".$this ->nombre."'";
    }
    public function consultarE(){
        return "select nombre, cantidad, precio, foto
                from Producto
                where idProducto = '" . $this -> idProducto .  "'";
    }
    public function editarE(){
        return "update Producto
                set nombre = '" . $this -> nombre . "', cantidad = '" . $this -> cantidad . "', precio = '" . $this -> precio . "'" . (($this -> Foto!="")?", foto = '" . $this -> foto . "'":"") . "
                where idProducto = '" . $this -> idProducto .  "'";
    }
}

?>