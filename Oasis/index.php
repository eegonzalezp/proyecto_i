<?php 
session_start();
require_once "Logica/Administrador.php";
require_once "Logica/Repartidor.php";
require_once "Logica/Producto.php";
require_once "Logica/Cliente.php";
require_once "Logica/Log.php";
require_once "Logica/ProductoF.php";
require_once "Logica/Factura.php";
require_once "Logica/Envio.php";


$pid = "";
if(isset($_GET["pid"])){
    $pid = base64_decode($_GET["pid"]);    
}else{
    $_SESSION["id"]="";
	$_SESSION["rol"]="";
	$_SESSION["carrito"]="";
}
if(isset($_GET["cerrarSesion"]) || !isset($_SESSION["id"])){
    $_SESSION["id"]="";
}
?>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" > <!-- link de bootstrap -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" /><!-- link de font awesome -->
    <script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script><!-- link de ionicons -->
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" ></script><!-- link de jquery -->
	<script src="funciones.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" ></script>
    <link rel="icon" type="image/png" href="img/logo.png"/><!-- icono -->
	<title>Oasis</title>
	<script>
	$(function () {
		  $('[data-toggle="tooltip"]').tooltip()
	})
	</script>
</head>
<body>
<?php 
	 	
		 $paginasSinSesion = array(
			 "Presentacion/autenticar.php",
			 "Presentacion/cliente/registrarCliente.php",
			 "Presentacion/login.php",
			 "Presentacion/inicioTienda.php",
			
			);	
			if(in_array($pid, $paginasSinSesion)){
				include $pid;
			}else if($_SESSION["id"]!="") {
				if($_SESSION["rol"] == "Administrador"){
					include "Presentacion/menuAdministrador.php";
				}else if($_SESSION["rol"] == "Cliente"){
					include "Presentacion/menuCliente.php";
				}else if($_SESSION["rol"] == "Repartidor"){
					include "Presentacion/menuRepartidor.php";
				}
					    
				include $pid;
			}else{
				include "Presentacion/navegacion.php";
				include "Presentacion/inicio.php";

			}
			include "Presentacion/footer.php";
			/* Aqui footer  archivo php */
			?>	


</body>
</html>

