<?php
 include 'Persistencia/conexion.php';
 include 'carrito.php';
?>
<br>
    <?php if($mensaje!=""){?>
 <div class="alert alert-success">
 <?php echo $mensaje; ?>
        
        <a href="mostrarCarrito.php" class="badge badge-success">Ver carrito</a>
        </div>
    <?php } ?>
   <div class="row">
    <?php
        $producto = new Producto(); 
        $listaProducto=$producto->consultarTodos();
    ?>
    <?php foreach($listaProductos as $producto){ ?>
        <div class="col-3">
           <div class="card">
               <img 
               title="<?php echo $producto->getNombre();?>"
               alt="<?php echo $producto->getNombre();?>"
               class="card-img-top"
               src="<?php echo $producto->getFoto();?>"
               data-toggle="popover" 
               data-trigger="hover"
               data-content="<?php echo $producto->getDescripcion();?>"
            height="200px"
               >
               <div class="card-body">
                <span><?php echo $producto->getNombre();?></span>
                   <h5 class="card-title">$<?php echo $producto->getPrecio();?></h5>
                   <p class="card-text">Descripcion</p>

                <form action="" method="post">

                       <input type="hidden" name="id" id="id" value="<?php echo $producto->getIdProducto();?>">
                       <input type="hidden" name="nombre" id="nombre" value="<?php echo $producto->getNombre();?>">
                       <input type="hidden" name="precio" id="precio" value="<?php echo $producto->getPrecio();?>">
                       <input type="hidden" name="cantidad" id="cantidad" value="<?php echo $producto->getCantidad(); ?>">
                       
                <button class="btn btn-primary"
                 name="btnAccion" 
                 value="Agregar" 
                 type="submit"
                 >
                 Agregar al carrito
                </button>

                </form>
                   
               </div>
           </div>

       </div>
  
     <?php } ?>  
 

    </div> 

    <script> 
    
    $(function () {
            $('[data-toggle="popover"]').popover()
    });
    
    </script>
