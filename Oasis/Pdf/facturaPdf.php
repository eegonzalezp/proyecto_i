
<?php
include "plantilla.php";
require "conexion.php";

$query = "SELECT idfactura, ciudad, barrio, direccion, telefono, estado from envio";
$resultado = $mysqli->query($query);

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();



$pdf->SetFillColor(255, 255, 255);

$pdf->SetFont('Arial', 'B', 12);

$pdf->Cell(30, 12, 'Factura', 0, 0, 'C', 1);
$pdf->Cell(30, 12, 'Ciudad', 0, 0, 'C', 1);
$pdf->Cell(30, 12, 'Barrio', 0, 0, 'C', 1);
$pdf->Cell(40, 12, 'Direccion', 0, 0, 'C', 1);
$pdf->Cell(30, 12, 'Telefono', 0, 0, 'C', 1);
$pdf->Cell(25, 12, 'Estado', 0, 1, 'C', 1);

$pdf->SetDrawColor(46, 204, 113);
$pdf->SetLineWidth(1);
$pdf->Line(10, 62, 195, 62);
$pdf->Ln(3);

$pdf->SetFillColor(255, 255, 255);
$pdf->SetTextColor(40,40,40);
$pdf->SetDrawColor(240, 240, 240);
$pdf->SetFont('Arial', '', 12);
while ($row = $resultado->fetch_assoc()) {
   $pdf->Cell(30, 8, $row['idfactura'], 1, 0, 'C', 1);
   $pdf->Cell(30, 8, $row['ciudad'], 1, 0, 'C', 1);
   $pdf->Cell(30, 8, $row['barrio'], 1, 0, 'C', 1);
   $pdf->Cell(40, 8, $row['direccion'], 1, 0, 'C', 1);
   $pdf->Cell(30, 8, $row['telefono'], 1, 0, 'C', 1);
   $pdf->Cell(25, 8, $row['estado'], 1, 1, 'C', 1);
}

/* 
 $pdf->SetFillColor(232,232,232);
 $pdf->SetFont('Arial', 'B',15);

 
 $query = "SELECT idRepartidor, nombre, apellido, correo, from repartidor";
 $resultado = $mysqli -> query($query);

 while ($row = $resultado->fetch_assoc()) {
     $pdf->Cell(30, 8, 'Generado por:', 1, 1, 'C', 1);
     $pdf ->Cell(30, 8, $rowR['nombre'.'apellido'], 0, 1, 'L');
     $pdf ->Cell(30, 8, $rowR['correo'], 0, 0, 'L');
 } */
$pdf->Output();
?>
