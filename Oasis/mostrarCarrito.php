<br>
<?php
$var = new  DateTime();
if (isset($_POST["btnEliminar"])) {
    $eliminar = new ProductoF($_POST['idp']);
    $eliminar->borrar();
}
if ($_SESSION['carrito'] == "") {
    $_SESSION['carrito'] = $var->getTimestamp();
}
$pedido = new ProductoF("", $_SESSION['carrito']);
$factura = $pedido->consultarTodos();
if (count($factura) == 0) {
    echo "<div class='alert alert-danger container'> Aun no se han agregado productos</div>";
}

?>
<?php if (isset($_POST["btnComprar"])) {
?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        Compra realizada
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
<?php } ?>

<body style="background-image: url(img/fondoJuice.jpg);
background-size: cover">

    <div class="container mt-3">
        
        <div class="card">
            <div class="card-header bg-dark text-white"><h3><i class="fas fa-shopping-cart"></i> Carrito</h3></div>
            <div class="card-body">
                <table class="table table-striped table-bordered">

                    <tbody>
                        <tr>
                            <th width="40%">Descripcion</th>
                            <th width="15%" class="text-center">Cantidad</th>
                            <th width="20%" class="text-center">Precio</th>
                            <th width="20%" class="text-center">Total</th>
                            <th width="50%">----</th>
                        </tr>
                        <?php $total = 0; ?>
                        <?php foreach ($factura as $producto) {

                        ?>
                            <tr>
                                <td width="40%"><?php echo $producto->getNombre(); ?></td>
                                <td width="15%" class="text-center"><?php echo $producto->getCantidad(); ?></td>
                                <td width="20%" class="text-center"><?php echo $producto->getPrecio() ?></td>
                                <td width="20%" class="text-center"><?php echo number_format($producto->getPrecio() * $producto->getCantidad(), 0); ?></td>
                                <td width="5%">




                                    <form action="index.php?pid=<?php echo base64_encode("mostrarCarrito.php") ?>" method="post">
                                        <input type="hidden" name="idp" id="id" value="<?php echo $producto->getIdProducto(); ?>">
                                        <button class="btn btn-danger" type="submit" name="btnEliminar" value="Eliminar"><i class="fas fa-times-circle"></i></button>
                                    </form>


                                </td>
                            </tr>
                            <?php $total = $total + ($producto->getPrecio() * $producto->getCantidad()); ?>
                        <?php } ?>
                        <tr>
                            <td colspan="3" align="right">
                                <h3>Total</h3>
                            </td>
                            <td align="right">
                                <h3>$<?php echo number_format($total); ?></h3>
                            </td>
                            <td></td>
                        </tr>

                        <tr>
                            <td colspan=5>


                                <form action="index.php?pid=<?php echo base64_encode("Presentacion/menuEnvios.php") ?>" method="post">
                                    <button class="btn btn-success btn-lg btn-block" type="submit" name="btnComprar" value="proceder">
                                    <i class="fas fa-check-circle"></i> Confirmar compra
                                    </button>
                                </form>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>


    </div>
    <div>
        <br><br>
    </div>
</body>
<!-- <div class="alert alert-success">
    No hay productos en el carrito...
</div> -->