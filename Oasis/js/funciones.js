function agregaform(datos){

	d=datos.split('||');

	$('#idCliente').val(d[0]);
	$('#nombre').val(d[1]);
	$('#apellido').val(d[2]);
	$('#correo').val(d[3]);
	$('#estado').val(d[4]);
	
}
function agregaformRepartidor(datos){

	d=datos.split('||');

	$('#idfactura').val(d[0]);
	$('#ciudad').val(d[1]);
	$('#barrio').val(d[2]);
	$('#direccion').val(d[3]);
	$('#telefono').val(d[4]);
	$('#estado').val(d[5]);
	
}
function agregaformLog(datos){

	d=datos.split('||');

	$('#id').val(d[0]);
	$('#accion').val(d[1]);
	$('#datos').val(d[2]);
	$('#fecha').val(d[3]);
	$('#hora').val(d[4]);
	$('#actor').val(d[5]);
	
}


function actualizaDatos(){


	idCliente=$('#idCliente').val();
	nombre=$('#nombre').val();
	apellido=$('#apellido').val();
	correo=$('#correo').val();
	estado=$('#estado').val();

	cadena= "idCliente=" + idCliente +
			"&nombre=" + nombre + 
			"&apellido=" + apellido +
			"&correo=" + correo +
			"&estado=" + estado;

	$.ajax({
		type:"POST",
		url:"Presentacion/cliente/php/actualizaDatos.php",
		data:cadena,
		success:function(r){
			
			if(r==1){
				$('#tabla').load('Presentacion/cliente/componentes/tabla.php');
				alertify.success("Datos actualizados");
			}else{
				alertify.error("Error en el servidor");
			}
		}
	});
	

}
function enviarPedido(){


	idfactura=$('#idfactura').val();
	ciudad=$('#ciudad').val();
	barrio=$('#barrio').val();
	direccion=$('#direccion').val(); 
	telefono=$('#telefono').val(); 
	estado=$('#estado').val();

	cadena= "idfactura=" + idfactura +
			"&ciudad=" + ciudad + 
			"&barrio=" + barrio +
			"&direccion=" + direccion + 
			"&telefono=" + telefono +
			"&estado=" + estado;

	$.ajax({
		type:"POST",
		url:"Presentacion/repartidor/php/enviarPedido.php",
		data:cadena,
		success:function(r){
			
			if(r==1){
				$('#tablaRepartidor').load('Presentacion/repartidor/componentes/tablaRepartidor.php');
				alertify.success("Pedido enviado");
			}else{
				alertify.error("Error en el servidor :(");
			}
		}
	});

}