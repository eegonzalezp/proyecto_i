<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/ProductoFDAO.php";
class ProductoF{
    private $idProducto;
    private $idfactura;
    private $nombre;
    private $cantidad;
    private $precio;
    private $conexion;
    private $productoFDAO;
    
    public function getIdProducto(){
        return $this -> idProducto;
    }
    public function getIdfactura(){
        return $this -> idfactura;
    }
    
    public function getNombre(){
        return $this -> nombre;
    }
    
    public function getCantidad(){
        return $this -> cantidad;
    }
    
    public function getPrecio(){
        return $this -> precio;
    }
        
    public function ProductoF($idProducto = "", $idfactura = "", $nombre = "", $cantidad = "", $precio = ""){
        $this -> idProducto = $idProducto;
        $this -> idfactura = $idfactura;
        $this -> nombre = $nombre;
        $this -> cantidad = $cantidad;
        $this -> precio = $precio;
        $this -> conexion = new Conexion();
        $this -> productoFDAO = new ProductoFDAO($this -> idProducto,$this -> idfactura, $this -> nombre, $this -> cantidad, $this -> precio);
    }
    
    public function insertar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> productoFDAO -> insertar());        
        $this -> conexion -> cerrar();        
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoFDAO -> consultarTodos());
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new ProductoF($resultado[0],"", $resultado[1], $resultado[2], $resultado[3], $resultado[4]);
            array_push($productos, $p);
        }
        $this -> conexion -> cerrar();        
        return $productos;
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> productoFDAO -> consultarPaginacion($cantidad, $pagina));
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new ProductoF($resultado[0],"", $resultado[1], $resultado[2], $resultado[3]);
            array_push($productos, $p);
        }
        $this -> conexion -> cerrar();
        return $productos;
    }
    
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoFDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    public function consultarCantidad2(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoFDAO -> consultarCantidad2());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    public function borrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoFDAO -> borrar());
        $this -> conexion -> cerrar();
    }
    
    
}

?>