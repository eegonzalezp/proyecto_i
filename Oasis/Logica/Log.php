<?php
    require_once "Persistencia/LogDao.php";
    require_once "Persistencia/Conexion.php";

    class Log
    {
        private $id;
        private $accion;
        private $datos;
        private $fecha;
        private $hora;
        private $actor;
        private $LogDao;
        private $conexion;
    
        public function getId()
        {
            return $this -> id;
        }

        public function getAccion()
        {
            return $this -> accion;
        }

        public function getDatos()
        {
            return $this -> datos;
        }

        public function getFecha()
        {
            return $this -> fecha;
        }

        public function getHora()
        {
            return $this -> hora;
        }

        public function getActor()
        {
            return $this -> actor;
        }

        public function Log($id = "", $accion = "", $datos = "", $fecha = "", $hora = "", $actor = ""){
            $this -> id = $id;
            $this -> accion = $accion;
            $this -> datos = $datos;
            $this -> fecha = $fecha;
            $this -> hora = $hora;
            $this -> actor = $actor;
            
            $this -> conexion = new Conexion();
            $this -> LogDao = new LogDao($this -> id, $this -> accion, $this -> datos, $this -> fecha, $this -> hora, $this -> actor);
            
        }
        public function insertarLog(){
                  
            $this -> conexion -> abrir();        
            $this -> conexion -> ejecutar($this -> LogDao -> insertarLog()); 
            $this -> conexion -> cerrar();        
        }
        public function consultarFiltro($filtro){
            $this -> conexion -> abrir();        
            $this -> conexion -> ejecutar($this -> LogDao -> consultarFiltro($filtro));
            $logs = array();
            while(($resultado = $this -> conexion -> extraer()) != null){
                $L = new Log($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5]);
                array_push($logs, $L);
            }
            $this -> conexion -> cerrar();
            return $logs;
        }

    }

?>
