<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/EnvioDAO.php";
class Envio
{
    private $idEnvio;
    private $idfactura;
    private $ciudad;
    private $barrio;
    private $direccion;
    private $telefono;
    private $estado;
    private $conexion;
    private $envioDAO;

    public function getIdEnvio()
    {
        return $this -> idEnvio;
    }

    public function getIdfactura(){

        return $this -> idfactura;

    }

    public function getCiudad()
    {
        return $this -> ciudad;
    }

    public function getBarrio()
    {
        return $this -> barrio;
    }

    public function getDireccion()
    {
        return $this -> direccion;
    }

    public function getTelefono()
    {
        return $this -> telefono;
    }

    public function getEstado()
    {
        return $this -> estado;
    }
    
    public function Envio($idEnvio = "", $idfactura = "" , $ciudad = "", $barrio = "", $direccion = "", $telefono = "", $estado = ""){
        $this -> idEnvio = $idEnvio;
        $this -> idfactura = $idfactura;
        $this -> ciudad = $ciudad;
        $this -> barrio = $barrio;
        $this -> direccion = $direccion;
        $this -> telefono = $telefono;
        $this -> estado = $estado;
        $this -> conexion = new Conexion();
        $this -> envioDAO = new EnvioDAO(
            $this -> idEnvio,
            $this -> idfactura,
            $this -> ciudad,
            $this -> barrio,
            $this -> direccion,
            $this -> telefono,
            $this -> estado
        );
    }
    public function insertarEnvio(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> envioDAO -> insertarEnvio());        
        $this -> conexion -> cerrar();        
    }
  
}
?>
