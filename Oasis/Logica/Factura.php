<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/FacturaDAO.php";
class Factura{
    private $idcliente;
    private $idfactura;
    private $nombre;
    private $cantidad;
    private $precio;
    private $conexion;
    private $FacturaDAO;
    
    public function getIdcliente(){
        return $this -> idcliente;
    }
    public function getIdfactura(){
        return $this -> idfactura;
    }
    
    public function getNombre(){
        return $this -> nombre;
    }
    
    public function getCantidad(){
        return $this -> cantidad;
    }
    
    public function getPrecio(){
        return $this -> precio;
    }
        
    public function Factura($idcliente = "", $idfactura = "", $nombre = "", $cantidad = "", $precio = ""){
        $this -> idcliente = $idcliente;
        $this -> idfactura = $idfactura;
        $this -> nombre = $nombre;
        $this -> cantidad = $cantidad;
        $this -> precio = $precio;
        $this -> conexion = new Conexion();
        $this -> FacturaDAO = new FacturaDAO($this -> idcliente, $this -> idfactura, $this -> nombre, $this -> cantidad, $this -> precio);
    }
    
    public function insertar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> FacturaDAO -> insertar());        
        $this -> conexion -> cerrar();        
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> FacturaDAO -> consultarTodos());
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Producto($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4]);
            array_push($productos, $p);
        }
        $this -> conexion -> cerrar();        
        return $productos;
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> FacturaDAO -> consultarPaginacion($cantidad, $pagina));
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new ProductoF($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4]);
            array_push($productos, $p);
        }
        $this -> conexion -> cerrar();
        return $productos;
    }
    
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> FacturaDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    
}

?>