<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/ProductoDAO.php";
class Producto{
    private $idProducto;
    private $nombre;
    private $cantidad;
    private $precio;
    private $Descripcion;
    private $Foto;
    private $conexion;
    private $productoDAO;
    
    public function getIdProducto(){
        return $this -> idProducto;
    }
    
    public function getNombre(){
        return $this -> nombre;
    }
    
    public function getCantidad(){
        return $this -> cantidad;
    }
    
    public function getPrecio(){
        return $this -> precio;
    }
    public function getDescripcion(){
        return $this -> descripcion;
    }
    public function getFoto(){
        return $this -> foto;
    }
        
    public function Producto($idProducto = "", $nombre = "", $cantidad = "", $precio = "", $Descripcion="", $Foto=""){
        $this -> idProducto = $idProducto;
        $this -> nombre = $nombre;
        $this -> cantidad = $cantidad;
        $this -> precio = $precio;
        $this -> descripcion = $Descripcion;
        $this -> foto = $Foto;
        $this -> conexion = new Conexion();
        $this -> productoDAO = new ProductoDAO($this -> idProducto, $this -> nombre, $this -> cantidad, $this -> precio, $this -> descripcion, $this -> foto);
    }
    
    public function insertar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> productoDAO -> insertar());        
        $this -> conexion -> cerrar();        
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarTodos());
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Producto($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5]);
            array_push($productos, $p);
        }
        $this -> conexion -> cerrar();        
        return $productos;
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarPaginacion($cantidad, $pagina));
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Producto($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5]);
            array_push($productos, $p);
        }
        $this -> conexion -> cerrar();
        return $productos;
    }
    
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    public function editar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> editar());
        $this -> conexion -> cerrar();
    }
    public function consultar2(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> consultar2());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idProducto = $resultado[0];
        $this -> nombre = $resultado[1];
        $this -> cantidad = $resultado[2];
        $this -> precio = $resultado[3];
    }
    public function consultarE(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarE());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> cantidad = $resultado[1];
        $this -> precio = $resultado[2];
        $this -> Foto = $resultado[3];
    }
    public function editarE(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> editarE());
        $this -> conexion -> cerrar();
    }
}

?>