<footer class="footer text-center py-5">
    
      <div class="col-md-12 text-center">
        <h3 class="text-success">Contáctenos</h3>
        <p class="lead">
          <i class="fas fa-envelope-square"></i> oasiservicioalcliente@gmail.com <br>
          <i class="fas fa-phone-square-alt"></i> 3045432551 <br>
          <i class="fas fa-concierge-bell"></i> Todos los dias: 06:00 - 21:00 <br>
          <a href="index.php"><i class="fab fa-facebook-square"></i></a>
          <a href="index.php"><i class="fab fa-instagram"></i></a>
        </p>
      </div>
  
   
    <div class="container-fluid" id="footerDown">
      
      <p class="text-center">OASIS Pulpa natural &copy; Copyright 2020 Todos los derechos reservados</p>
    </div>
    
  </footer>