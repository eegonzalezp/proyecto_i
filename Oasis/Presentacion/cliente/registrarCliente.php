<?php
include "Presentacion/navegacion.php";

$error = 0;
$registrado = false;
if (isset($_POST["registrar"])) {
	$nombre = $_POST["nombre"];
	$apellido = $_POST["apellido"];
	$correo = $_POST["correo"];
	$clave = $_POST["clave"];
	$estado = 1;
	$cliente = new Cliente("", $nombre, $apellido, $correo, $clave, $estado);
	if ($cliente->existeCorreo()) {
		$error = 1;
		$registrado = true;
	} else {
		$cliente->registrar();
		$registrado = true;
	}
	$cliente = new Cliente($_SESSION['id']);
	$cliente->consultar();

	$Log = new Log("", "Se registró", $nombre . " " . $apellido, "NOW()", "NOW()", "Cliente");
	$Log->insertarLog();
}

?>


<link href="Style/design.css" rel="stylesheet">

<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">

<head>
    <link rel="stylesheet" type="text/css" href="librerias/alertifyjs/css/alertify.css">
    <link rel="stylesheet" type="text/css" href="librerias/alertifyjs/css/themes/default.css">
    <script src="librerias/alertifyjs/alertify.js"></script>
    <link rel="stylesheet" type="text/css" href="librerias/select2/css/select2.css">

    <!--JQUERY-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Nuestro css-->
    <link rel="stylesheet" type="text/css" href="css/index2.css" th:href="@{/css/index2.css}">

</head>

<body style="background-image: url(img/fondoFrutas.jpg);
 background-size:cover;">
    

    <div class="modal-dialog text-center">
        <div class="col-sm-8 main-section">
            <div class="modal-content ">
                <div class="col-12 text-white text-center "><h1><i class="fas fa-user-plus"></i><br> Registro</h1>
                    
                </div>
                <div class="card-body">
                    <!-- para llamar autenticacion toca desde la carpeta -->
                    <form action="index.php?pid=<?php echo base64_encode("Presentacion/cliente/registrarCliente.php") ?>" method="post">
                        <!-- para proteger la pagina -->
                        <div class="form-group">
							<input name="nombre" type="text" class="form-control" placeholder="Nombre" required>
						</div>
						<div class="form-group">
							<input name="apellido" type="text" class="form-control" placeholder="Apellido" required>
						</div>
						<div class="form-group">
							<input name="correo" type="email" class="form-control" placeholder="Correo" required>
						</div>
						<div class="form-group">
							<input name="clave" type="password" class="form-control" placeholder="Contraseña" required>
						</div>
                        <div class="form-group">
                            <input name="registrar" type="submit" class="form-control btn btn-primary text-left" value="Registrarse">
						</div>
					</form>
					<?php if ($error == 1) { ?>
						<script>
							alertify.error("Este correo ya está en uso");
						</script>
					<?php } else if ($registrado) { ?>
						<script>
							alertify.success("Registro exitoso");
						</script>
						
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>