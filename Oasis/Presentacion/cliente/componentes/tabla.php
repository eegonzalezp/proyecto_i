<?php
$conexion = mysqli_connect('localhost', 'root', '', 'bdoasis');
?>

<head>

	<link href="Style/design.css" rel="stylesheet">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"> <!-- link de bootstrap -->
	<script src="js/funciones.js"></script>
</head>



	<div class="container ">
		<div class="row">

			<div class="col-sm-12 mt-5">
				<div class="card">
					<div class="card-header text-white bg-dark text-center">
					<h2 class="text-center"><i class="fas fa-clipboard-list"></i> Lista de clientes</h2>
					</div>
					<div class="card-body">

						<table class="table table-hover table-condensed table-bordered tabla  table-striped" id="tabla" name="tabla">

							<tr class="bg-warning blockquote text-center">
								<td class="text-center">Id</td>
								<td class="text-center">Nombre</td>
								<td class="text-center">Apellido</td>
								<td class="text-center">Correo</td>
								<td class="text-center">estado</td>
								<td class="text-center">Editar</td>
							</tr>

							<?php
							$sql = "SELECT idCliente,nombre,apellido,correo,estado from cliente";
							$result = mysqli_query($conexion, $sql);

							while ($mostrar = mysqli_fetch_array($result)) {

								$datos = $mostrar['idCliente'] . "||" .
									$mostrar['nombre'] . "||" .
									$mostrar['apellido'] . "||" .
									$mostrar['correo'] . "||" .
									$mostrar['estado']
							?>
								<td class="text-center"><?php echo $mostrar['idCliente'] ?></td>
								<td class="text-center"><?php echo $mostrar['nombre'] ?></td>
								<td class="text-center"><?php echo $mostrar['apellido'] ?></td>
								<td class="text-center"><?php echo $mostrar['correo'] ?></td>
								<td class="text-center"><?php echo $mostrar['estado'] ?></td>
								<td class="text-center">
									<button class="btn btn-warning glyphicon glyphicon-pencil" data-toggle="modal" data-target="#modalEdicion"
									 onclick="agregaform('<?php echo $datos ?>')"><i class="far fa-edit"></i>

									</button>
								</td>

								</tr>
							<?php
							}
							?>
						</table>
					</div>
				</div>
			</div>

		</div>
		<div>
			<br><br>
		</div>
	</div>
