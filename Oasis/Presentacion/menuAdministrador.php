<?php
$administrador = new Administrador($_SESSION["id"]);
$administrador->consultar();
?>


<head>
	<link href="Style/design.css" rel="stylesheet">
</head>

<body class="fondo">
	<nav class="navbar navbar-expand-lg ">
		<div class="container text-dark">
			<a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("Presentacion/sesionAdministrador.php") ?>">
			<strong class="text-dark"><img src="img/logo.png"class="mb-5" style="height: 80px;"><span class="display-4 ">O A S I S </span></strong></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				
				<ul class="navbar-nav ml-auto text-light">
					<a class="nav-link" href="index.php?pid=<?php echo base64_encode("Presentacion/sesionAdministrador.php") ?>"><i class="fas fa-home"></i> Inicio</a>
					<a class="nav-link" href="index.php?pid=<?php echo base64_encode("Presentacion/cliente/indexCliente.php") ?>"><i class="far fa-address-book"></i> Clientes</a>
					<a class="nav-link" href="index.php?pid=<?php echo base64_encode("Presentacion/logAdministrador.php") ?>"><i class="fas fa-clipboard-list"></i> Actividad</a>
					
					<li class="nav-item dropdown active"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-box-open"></i> Producto</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/producto/crearProducto.php") ?>"><i class="far fa-plus-square"></i> Registrar pulpa</a>
							<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/producto/listaPulpas.php") ?>"><i class="fas fa-list-ul"></i> Lista de pulpas</a>
							<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/producto/consultarProductoTodos.php") ?>"><i class="fas fa-list-ul"></i> Todas las pulpas</a>
							
						</div>
					</li>
					<li class="nav-item dropdown active"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-user-circle"></i> <?php echo ($administrador->getNombre() != "" ? $administrador->getNombre() : $administrador->getCorreo()) ?> <?php echo $administrador->getApellido() ?></a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/perfilAdministrador.php") ?>"><i class="fas fa-user"></i> Ver perfil</a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="index.php?cerrarSesion=true"><i class="fas fa-sign-out-alt"></i> Cerrar Sesion</a>
							
						</div>
					</li>

				</ul>
			</div>
</body>

</nav>