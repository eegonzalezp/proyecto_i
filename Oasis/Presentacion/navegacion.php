<nav class="navbar navbar-expand-lg ">
    <div class="container text-dark">
        <a class="navbar-brand" href="index.php">
            <strong class="text-dark"><img src="img/logo.png"class="mb-5" style="height: 80px;"><span class="display-4 ">O A S I S </span></strong></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <ion-icon name="menu-outline"></ion-icon>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto text-light">

                <li class="nav-item active ">
                    <a class="nav-link" href="index.php"><i class="fas fa-home"></i> Inicio <span class="sr-only ">(current)</span></a>
                </li>
                <li class="nav-item active ">
                    <a class="nav-link" href="index.php?pid=<?php echo base64_encode("Presentacion/inicioTienda.php") ?>"><i class="fas fa-store"></i> Tienda <span class="sr-only ">(current)</span></a>
                </li>
             
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-user-circle"></i> Cuenta
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/login.php") ?>"><i class="fas fa-sign-in-alt"></i> Iniciar sesión</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/cliente/registrarCliente.php") ?>">
                            <i class="fas fa-user-plus"></i> Registrarme</a>


                    </div>

                </li>
            </ul>
        </div>
    </div>
</nav>