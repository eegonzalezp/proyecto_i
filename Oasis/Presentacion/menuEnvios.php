<?php
$idfactura = $_SESSION['carrito'];
$ciudad = "";
if (isset($_POST["ciudad"])) {
    $ciudad = $_POST["ciudad"];
}
$barrio = "";
if (isset($_POST["barrio"])) {
    $barrio = $_POST["barrio"];
}
$direccion = "";
if (isset($_POST["direccion"])) {
    $direccion = $_POST["direccion"];
}
$telefono = "";
if (isset($_POST["telefono"])) {
    $telefono = $_POST["telefono"];
}
$estado = "Pendiente";

if (isset($_POST["btnEnvio"])) {
    $pedido = new ProductoF("", $_SESSION['carrito']);
    $factura = $pedido->consultarTodos();
    foreach ($factura as $facturaActual) {
        $otro = new Producto($facturaActual->getIdProducto(), $facturaActual->getNombre());
        $otro->consultar2();
        
        $Fab = new Producto($otro->getIdProducto(), "", $otro->getCantidad() - $facturaActual->getCantidad());
        $Fab->editar();
    }
    $envio = new Envio("", $idfactura, $ciudad, $barrio, $direccion, $telefono, $estado);
    $envio->insertarEnvio();

    /* echo $idfactura . $ciudad . $barrio . $direccion . $telefono . $estado; */
}
?>


<head>
    <link rel="stylesheet" type="text/css" href="librerias/alertifyjs/css/alertify.css">
    <link rel="stylesheet" type="text/css" href="librerias/alertifyjs/css/themes/default.css">
    <script src="librerias/alertifyjs/alertify.js"></script>
    <link rel="stylesheet" type="text/css" href="librerias/select2/css/select2.css">
</head>

<body style="background-image: url(img/fondoBerrie.jpg);
background-size: cover">

</body>

    <form action="index.php?pid=<?php echo base64_encode("Presentacion/menuEnvios.php") ?>" method="post">
        <div class="container">
            <div class="card mt-5">
                <div class="card-header bg-dark text-white">
                    <h1 class="text-center"><i class="far fa-envelope"></i> Menu de envio</h1>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        
                        <select class="form-control " name="ciudad" placeholder="Ciudad" value="<?php echo $ciudad ?>" required>>
                            <option>Bogota</option>
                            <option>Medellin</option>
                            <option>Ibague</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="text" name="barrio" class="form-control" placeholder="Barrio" value="<?php echo $barrio ?>" required>

                    </div>
                    <div class="form-group">
                        <input type="text" name="direccion" class="form-control" placeholder="Dirección" value="<?php echo $direccion ?>" required>
                    </div>
                    <div class="form-group">
                        <input type="text" name="telefono" class="form-control" placeholder="Teléfono" value="<?php echo $telefono ?>" required>
                    </div>


                    <button type="submit" name="btnEnvio" class="btn btn-success btn-block" class="form-group"><i class="fas fa-check-circle"></i> Confirmar envío</button>
                </div>
            </div>



        </div>
    </form>

    <?php if (isset($_POST["btnEnvio"])) { ?>
        <script>
            alertify.success("Tu envío ha sido programado! Pronto enviaremos tu pedido");
        </script>
        <?php
        $cliente = new Cliente($_SESSION['id']);
        $cliente->consultar();

        $Log = new Log("", "Confirmó una compra", $cliente->getNombre() . " " . $cliente->getApellido(), "NOW()", "NOW()", "Cliente");
        $Log->insertarLog();

        ?>

    <?php } ?>
    </div>


