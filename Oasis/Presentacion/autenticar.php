<?php
$correo = $_POST["correo"];
$clave = $_POST["clave"];
$administrador = new Administrador("", "", "", $correo, $clave);
$cliente = new Cliente("", "", "", $correo, $clave);
$repartidor = new Repartidor ("", "", "", $correo, $clave); 
if($administrador -> autenticar()){
    $_SESSION["id"] = $administrador -> getIdAdministrador();
    $_SESSION["rol"] = "Administrador";

    $administrador = new Administrador($_SESSION['id']);
    $administrador -> consultar();

    $Log = new Log ("", "Inició sesión" ,$administrador->getNombre() ." " . $administrador->getApellido(),"NOW()","NOW()","Administrador");
    $Log -> insertarLog() ;

    header("Location: index.php?pid=" . base64_encode("Presentacion/sesionAdministrador.php"));
 }else if($cliente -> autenticar()){
    if($cliente -> getEstado() == -1){
        header("Location: index.php?error=2");
    }else if($cliente -> getEstado() == 0){
        header("Location: index.php?error=3");        
    }else{
        $_SESSION["id"] = $cliente -> getIdCliente();
        $_SESSION["rol"] = "Cliente";

    $cliente = new Cliente($_SESSION['id']);
    $cliente -> consultar();

    $Log = new Log ("", "Inició sesión" ,$cliente->getNombre()." ". $cliente->getApellido() ,"NOW()","NOW()","Cliente");
    $Log -> insertarLog() ;

        header("Location: index.php?pid=" . base64_encode("Presentacion/sesionCliente.php"));
    }
}else if($repartidor -> autenticar()){
    $_SESSION["id"] = $repartidor -> getIdRepartidor();
    $_SESSION["rol"] = "Repartidor";

    $repartidor = new Repartidor($_SESSION['id']);
    $repartidor -> consultar();

    $Log = new Log ("", "Inició sesión" ,$repartidor->getNombre()." ". $repartidor->getApellido(),"NOW()","NOW()","Repartidor");
    $Log -> insertarLog() ;

    header("Location: index.php?pid=" . base64_encode("Presentacion/sesionRepartidor.php")); 
}else{
    header("Location: index.php?error=1");
    

}
