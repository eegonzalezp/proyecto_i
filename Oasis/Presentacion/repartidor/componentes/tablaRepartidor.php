<?php
$conexion = mysqli_connect('localhost', 'root', '', 'bdoasis');
?>

<head>

	<link href="Style/design.css" rel="stylesheet">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"> <!-- link de bootstrap -->
	<script src="js/funciones.js"></script>
</head>

<body class="fondo">

	<div class="container bg-white">
		<div class="row">

			<div class="col-sm-12">
				<div class="card">
					<div class="card-header bg-dark text-white text-center">
						<h2 class="text-center"><i class="far fa-envelope"></i> Lista de envíos</h2>
					</div>
					<div class="card-body">
						<table class="table table-hover table-condensed table-bordered tabla" id="tablaRepartidor" name="tablaRepartidor">

							<tr class="bg-dark text-white">
								<td class="text-center">Factura</td>
								<td class="text-center">Ciudad</td>
								<td class="text-center">Barrio</td>
								<td class="text-center">Dirección</td>
								<td class="text-center">Teléfono</td>
								<td class="text-center">Estado</td>
								<td class="text-center">Información</td>

							</tr>

							<?php
							$sql = "SELECT idfactura, ciudad, barrio, direccion, telefono, estado from envio";
							$result = mysqli_query($conexion, $sql);

							while ($mostrar = mysqli_fetch_array($result)) {

								$datos = $mostrar['idfactura'] . "||" .
									$mostrar['ciudad'] . "||" .
									$mostrar['barrio'] . "||" .
									$mostrar['direccion'] . "||" .
									$mostrar['telefono'] . "||" .
									$mostrar['estado']
							?>
								<td class="text-center"><?php echo $mostrar['idfactura'] ?></td>
								<td class="text-center"><?php echo $mostrar['ciudad'] ?></td>
								<td class="text-center"><?php echo $mostrar['barrio'] ?></td>
								<td class="text-center"><?php echo $mostrar['direccion'] ?></td>
								<td class="text-center"><?php echo $mostrar['telefono'] ?></td>
								<td class="text-center"><?php echo $mostrar['estado'] ?></td>
								<td class="text-center">
									<button class="btn btn-primary" data-toggle="modal" data-target="#modalEnvio" 
									onclick="agregaformRepartidor('<?php echo $datos ?>')"><i class="fas fa-envelope"></i>

									</button>

								</td>


								</tr>
							<?php
							}
							?>
						</table>
						
					 <a class="btn btn-success btn-block" href="Pdf/indexPdf.php?pid=<?php echo base64_encode("facturaPdf.php") ?>" target="_blank"><i class="fas fa-file-download"></i> Generar reporte</a> 
					</div>
				</div>


			</div>

		</div>

	</div>


</body>