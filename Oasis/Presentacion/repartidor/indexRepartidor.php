<!DOCTYPE html>
<html>


<head>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">


	<link rel="stylesheet" type="text/css" href="librerias/alertifyjs/css/alertify.css">
	<link rel="stylesheet" type="text/css" href="librerias/alertifyjs/css/themes/default.css">
	<script src="librerias/alertifyjs/alertify.js"></script>
	<link rel="stylesheet" type="text/css" href="librerias/select2/css/select2.css">

	<script src="librerias/jquery-3.2.1.min.js"></script>
	<script src="js/funciones.js"></script>

	
	<script src="librerias/select2/js/select2.js"></script>
</head>

<body style="background-image: url(img/fondoJuice.jpg);
background-size: cover">
	
</body>

	<div class="container">

		<div id="tablaRepartidor"></div>

	</div>



	<!-- Modal para edicion de datos -->

	<div class="modal fade" id="modalEnvio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Info. del pedido</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>
				<div class="modal-body">

					<label>Factura</label>
					<input type="text" name="" id="idfactura" class="form-control input-sm">
					<label>Ciudad</label>
					<input type="text" name="" id="ciudad" class="form-control input-sm">
					<label>Barrio</label>
					<input type="text" name="" id="barrio" class="form-control input-sm">
					<label>Dirección</label>
					<input type="text" name="" id="direccion" class="form-control input-sm">
					<label>Teléfono</label>
					<input type="text" name="" id="telefono" class="form-control input-sm">
					<label>Estado</label>
					<input type="text" name="" id="estado" class="form-control input-sm">
				</div>
				<div class="dropdown-divider"></div>
				<div class="text-center">
					<button type="button" class="btn btn-primary " id="btnEnviar" data-dismiss="modal"><i class="fas fa-check-circle"></i> Enviar</button>
					<button type="button" class="btn btn-danger" id="btnNoEnviar" data-dismiss="modal"><i class="far fa-times-circle"></i> Aun no</button>
				</div>
				<div class="mt-3"></div>
			</div>
		</div>
	</div>



</html>

<script type="text/javascript">
	$(document).ready(function() {
		var url = "indexAjax.php?pid=<?php echo base64_encode("Presentacion/repartidor/componentes/tablaRepartidor.php") ?>";
		$("#tablaRepartidor").load(url);
	});
</script>

<script type="text/javascript">
	$(document).ready(function() {

		$('#btnEnviar').click(function() {
			<?php
			$repartidor = new Repartidor($_SESSION['id']);
			$repartidor->consultar();

			$Log = new Log("", "Envió un pedido", $repartidor->getNombre() . " " . $repartidor->getApellido(), "NOW()", "NOW()", "Repartidor");
			$Log->insertarLog();
			?>
			enviarPedido();
		});
	});
</script>