<html>

<head>
    <link href="Style/design.css" rel="stylesheet">
</head>

<body style="background-image: url(img/fondoFrutas.jpg);
background-size: cover">


    <div class="container">

        <div class="card-body col-lg-6 col-md-8 col-sm-8 mx-auto bg-warning rounded-lg mt-5">
            <div class="card-header text-white text-center">
                <h1>Admin</h1>
            </div>
            <div class="row">
                <div class="col-lg-8 mx-auto col-md-8 col-sm-8">
                    <img src="<?php echo ($administrador->getFoto() != "") ? $administrador->getFoto() :
                                    "http://icons.iconarchive.com/icons/custom-icon-design/silky-line-user/512/user-setting-icon.png"; ?>" width="100%" class="img-thumbnail">
                </div>
            </div>
            <div class="dropdown-divider"></div>
            <div class="row">
                <div class="col">
                    <div class="card mx-auto col-lg-8 col-md-8 col-sm-12 bg-white">
                        <table class="table col-lg-8 col-md-8 col-sm8 table-borderless">
                            <tr>
                                <th>Nombre</th>
                                <td><?php echo $administrador->getNombre() ?></td>
                            </tr>
                            <tr>
                                <th>Apellido</th>
                                <td><?php echo $administrador->getApellido() ?></td>
                            </tr>
                            <tr>
                                <th>Correo</th>
                                <td><?php echo $administrador->getCorreo() ?></td>
                            </tr>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <br><br>

</body>
