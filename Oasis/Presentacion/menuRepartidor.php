<?php
$repartidor = new Repartidor($_SESSION["id"]);
$repartidor->consultar();
?>
<head>
	<link href="Style/design.css" rel="stylesheet">
</head>

<body class="fondo">
	<nav class="navbar navbar-expand-lg ">
		<div class="container text-dark">
			<a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("Presentacion/sesionRepartidor.php") ?>">
			<strong class="text-dark"><img src="img/logo.png"class="mb-5" style="height: 80px;"><span class="display-4 ">O A S I S </span></strong></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				
				<ul class="navbar-nav ml-auto text-light">
					<a class="nav-link" href="index.php?pid=<?php echo base64_encode("Presentacion/sesionRepartidor.php") ?>"><i class="fas fa-home"></i> Inicio</a>
					<a class="nav-link" href="index.php?pid=<?php echo base64_encode("Presentacion/repartidor/indexRepartidor.php") ?>"><i class="fas fa-shipping-fast"></i> Envios</a>
					
					<li class="nav-item dropdown active"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-user-circle"></i> <?php echo ($repartidor->getNombre() != "" ? $repartidor->getNombre() : $repartidor->getCorreo()) ?> <?php echo $repartidor->getApellido() ?></a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/perfilRepartidor.php") ?>"><i class="fas fa-user"></i> Ver perfil</a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="index.php?cerrarSesion=true"><i class="fas fa-sign-out-alt"></i> Cerrar Sesion</a>
						</div>
					</li>
				</ul>
			</div>
</body>

</nav>