<?php
include "Presentacion/navegacion.php";
?>

<head>
  <link href="Style/design.css" rel="stylesheet">
  <script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>
</head>



<body class="fondo">

  <div id="carouselExampleIndicators" class=" carousel slide" data-ride="carousel">

    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="img/envios.jpg" class="d-block w-100">
        <div class="info">
          <h2>!Hola!</h2>
          <a class="btn btn-dark" href="index.php?pid=<?php echo base64_encode("Presentacion/login.php") ?>">Ir a la tienda</a>
        </div>
      </div>
    </div>
  </div>

  <div class="container mt-5">
    <h2 class="text-center">PULPAS DE FRUTA</h2>
    <p class="lead text-center">
      Nada sabe mejor que lo auténtico, y la pulpa de fruta <span class="color_verde">OASIS</span> es <span class="color_verde">100 % Colombiana, 100 % Natural</span>.
    </p>

  </div>

  <div class="dropdown-divider"></div>

  <div class=" container mora mt-5">
    <div class="card-body  rounded-lg contenido-container">
      <div class="row">
        <div class="col-md-6 ml-auto ">
          <img src="img/mora_info.jpg" class="img-fluid  rounded-lg">
        </div>
        <div class="col-md-6 text-center text-dark">
          <div class="card-body bg-light rounded-lg">
            <h2>PULPA DE MORA - OASIS</h2>
            <p class="lead text-justify">
              Las moras, son deliciosas y sumamente nutritivas, estos son algunos de sus beneficios:
            </p>

            <ul class="lead text-left">
              <li>Anti inflamatorio natural</li>
              <li>Combate la fiebre</li>
              <li>Contiene vitaminas A Y C</li>
              <li>Fuente de fibra y hierro</li>
              <li>Vitamina C y K</li>
            </ul>
            <a class="btn btn-dark" href="index.php?pid=<?php echo base64_encode("Presentacion/login.php") ?>">Ver productos</a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="dropdown-divider"></div>
  <div class="container uva">
    <div class="card-body  rounded-lg contenido-container">
      <div class="row">
        <div class="col-md-6 ml-auto ">
          <img src="img/uva_info.jpg" class="img-fluid  rounded-lg">
        </div>
        <div class="col-md-6 text-center text-dark">
          <div class="card-body bg-light rounded-lg">
            <h2>PULPA DE UVA - OASIS</h2>
            <p class="lead text-justify">
              Es Ideal como aporte nutricional para quienes necesitan una buena fuente de energía
            </p>

            <ul class="lead text-left">
              <li>Vitamina C y entre sus minerales destacan el potasio, el cobre y el hierro</li>
              <li>Evita el estreñimiento </li>
              <li>Las uvas Contienen propiedades diuréticas</li>

            </ul>
            <a class="btn btn-dark" href="index.php?pid=<?php echo base64_encode("Presentacion/login.php") ?>">Ver productos</a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="dropdown-divider"></div>
  <div class="container fresa">
    <div class="card-body  rounded-lg contenido-container">
      <div class="row">
        <div class="col-md-6 ml-auto ">
          <img src="img/fresa_info.jpg" class="img-fluid  rounded-lg">
        </div>
        <div class="col-md-6 text-center text-dark">
          <div class="card-body bg-light rounded-lg">
            <h2>PULPA DE FRESA - OASIS</h2>
            <p class="lead text-justify">
              Nuestra pulpa de fresa cuenta con un aroma, color y sabor irresistibles,
              ademas de esto es una fruta con atributos nutricionales muy importantes como:
            </p>

            <ul class="lead text-left">
              <li>Rica en vitamina C</li>
              <li>Evita retencion de liquidos </li>
              <li>Alto contenido de Magnesio, Potasio y vitamina K</li>

            </ul>
            <a class="btn btn-dark" href="index.php?pid=<?php echo base64_encode("Presentacion/login.php") ?>">Ver productos</a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="dropdown-divider"></div>
  <div class="container maracuya">
    <div class="card-body  rounded-lg contenido-container">
      <div class="row">
        <div class="col-md-6 ml-auto ">
          <img src="img/maracuya_info.jpg" class="img-fluid  rounded-lg mt-2">
        </div>
        <div class="col-md-6 text-center text-dark">
          <div class="card-body bg-light rounded-lg">
            <h2>PULPA DE MARACUYA - OASIS</h2>
            <p class="lead text-justify">
              Es una Fruta de origen tropical que ademas de ser deliciosa posee muchas propiedades nutritivas y medicinales como:
            </p>

            <ul class="lead text-left">
              <li>Ayuda a manejar la diabetes tipo 2</li>
              <li>Previene enfermedades cardiovasculares </li>
              <li>Regula el nivel de glucosa en la sangre</li>
              <li>Rica en Vitamina A y vitamina C</li>

            </ul>
            <a class="btn btn-dark" href="index.php?pid=<?php echo base64_encode("Presentacion/login.php") ?>">Ver productos</a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="dropdown-divider"></div>
  <div class="container mango">
    <div class="card-body rounded-lg contenido-container">
      <div class="row">
        <div class="col-md-6 ml-auto ">
          <img src="img/mango_info.jpg" class="img-fluid  rounded-lg mt-2">
        </div>
        <div class="col-md-6 text-center text-dark">
          <div class="card-body bg-light rounded-lg">
            <h2>PULPA DE MANGO - OASIS</h2>
            <p class="lead text-justify">
              Nuestra pulpa de fruta es fabricada con mangos de la mas alta calidad con el fin de conservar sus atributos como:
            </p>

            <ul class="lead text-left">
              <li> Alto contenido de vitaminas A y C</li>
              <li>Rico en hierro </li>
              <li>Antioxidante</li>
              <li>Diurético</li>
            </ul>
            <a class="btn btn-dark" href="index.php?pid=<?php echo base64_encode("Presentacion/login.php") ?>">Ver productos</a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="dropdown-divider"></div>
  <div class="container lulo">
    <div class="card-body rounded-lg contenido-container">
      <div class="row">
        <div class="col-md-6 ml-auto ">
          <img src="img/lulo_info.jpg" class="img-fluid  rounded-lg">
        </div>
        <div class="col-md-6 text-center text-dark">
          <div class="card-body bg-light rounded-lg">
            <h2>PULPA DE LULO - OASIS</h2>
            <p class="lead text-justify">
              Nuestra pulpa de Lulo es excepcional para el consumo diario ya que logramos condensar sus atributos nutricionales como:
            </p>
            <ul class="lead text-left">
              <li>Rica en vitamina C</li>
              <li>Contiene vitamina A, Fosforo, Hierro y calcio</li>
              <li>Evita hipertencion y migraña</li>
            </ul>
            <a class="btn btn-dark" href="index.php?pid=<?php echo base64_encode("Presentacion/login.php") ?>">Ver productos</a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="dropdown-divider"></div>
  <div class="container guanabana">
    <div class="card-body rounded-lg contenido-container">
      <div class="row">
        <div class="col-md-6 ml-auto">
          <img src="img/guanaban_info.jpg" class="img-fluid rounded-lg">
        </div>
        <div class="col-md-6 text-center text-dark">
          <div class="card-body bg-light rounded-lg">
            <h2>PULPA DE GUANÁBANA - OASIS</h2>
            <p class="lead text-justify">
              La guanábana es una fruta ideal para consumir a diario, por su exquisito sabor, su valor nutricional y sus múltiples beneficios medicinales
            </p>
            <ul class="lead text-left">
              <li>Rica en calcio y fosforo</li>
              <li>Contiene vitamina C y Hierro</li>
              <li>Contiene vitamina B1</li>
            </ul>
            <a class="btn btn-dark" href="index.php?pid=<?php echo base64_encode("Presentacion/login.php") ?>">Ver productos</a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="dropdown-divider"></div>
  <div class="container guayaba">
    <div class="card-body rounded-lg contenido-container">
      <div class="row">
        <div class="col-md-6 ml-auto ">
          <img src="img/guayaba_info.jpg" class="img-fluid  rounded-lg mt-3">
        </div>
        <div class="col-md-6 text-center text-dark">
          <div class="card-body bg-light rounded-lg">
            <h2>PULPA DE GUAYABA - OASIS</h2>
            <p class="lead text-justify">
              La guayaba es una de las frutas más apreciadas y utilizadas dentro de nuestra
              gastronomía colombiana gracias a sus grandes beneficios y sus atributos nutricionales como:
            </p>
            <ul class="lead text-left">
              <li>Rica en vitaminas A, E Y D12</li>
              <li>Gran fuente de fibra natural</li>
              <li>Rica en vitamina C, hierro y calcio</li>
            </ul>
            <a class="btn btn-dark" href="index.php?pid=<?php echo base64_encode("Presentacion/login.php") ?>">Ver productos</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="dropdown-divider"></div>

</body>