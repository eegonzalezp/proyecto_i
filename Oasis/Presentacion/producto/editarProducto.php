<?php
if (isset($_POST["editar"])) {
	if ($_FILES["foto"]["name"] != "") {
		$rutaLocal = $_FILES["foto"]["tmp_name"];
		$tipo = $_FILES["foto"]["type"];
		$tiempo = new DateTime();
		$rutaRemota = "img/" . $tiempo->getTimestamp() . (($tipo == "image/png") ? ".png" : ".jpg");
		copy($rutaLocal, $rutaRemota);
		$producto = new Producto($_GET["idProducto"]);
		$producto->consultarE();
		if ($producto->getFoto() != "") {
			unlink($producto->getFoto());
		}
		$producto = new Producto($_GET["idProducto"], $_POST["nombre"], $_POST["cantidad"], $_POST["precio"], $rutaRemota);
		$producto->editarE();
	} else {
		$producto = new Producto($_GET["idProducto"], $_POST["nombre"], $_POST["cantidad"], $_POST["precio"]);
		$producto->editarE();

		$administrador = new Administrador($_SESSION['id']);
		$administrador->consultar();

		$Log = new Log("", "Editó una pulpa", $administrador->getNombre()." " . $administrador->getApellido(), "NOW()", "NOW()", "Administrador");
		$Log->insertarLog();
	}
} else {
	$producto = new Producto($_GET["idProducto"]);
	$producto->consultarE();

	
}
?>

<head>
	<link rel="stylesheet" type="text/css" href="librerias/alertifyjs/css/alertify.css">
	<link rel="stylesheet" type="text/css" href="librerias/alertifyjs/css/themes/default.css">
	<script src="librerias/alertifyjs/alertify.js"></script>
	<link rel="stylesheet" type="text/css" href="librerias/select2/css/select2.css">
</head>

<body style="background-image: url(img/fondoNaranjas.jpg);
 background-size:cover">


</body>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
			<div class="card">
				<div class="card-header text-white bg-dark text-center">
					<h2><i class="fas fa-edit"></i> Editar Pulpa</h2>
				</div>
				<div class="card-body">
					<?php if (isset($_POST["editar"])) { ?>
						<script>
							alertify.success("La pulpa se editó correctamente");
						</script>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/producto/editarProducto.php") ?>&idProducto=<?php echo $_GET["idProducto"] ?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nombre</label>
							<input type="text" name="nombre" class="form-control" value="<?php echo $producto->getNombre() ?>" required>
						</div>
						<div class="form-group">
							<label>Cantidad</label>
							<input type="number" name="cantidad" class="form-control" min="1" value="<?php echo $producto->getCantidad() ?>" required>
						</div>
						<div class="form-group">
							<label>Precio</label>
							<input type="number" name="precio" class="form-control" min="1" value="<?php echo $producto->getPrecio() ?>" required>
						</div>
						<div class="form-group">
							<label>Imagen</label>
							<input type="file" name="foto" class="form-control">
						</div>
						<button type="submit" name="editar" class="btn btn-success btn-block"><i class="fas fa-check-circle"></i> Editar</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div>
	<br><br>
</div>