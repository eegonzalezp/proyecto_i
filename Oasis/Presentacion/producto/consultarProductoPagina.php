<?php

$producto = new Producto();
$cantidad = 8;
if (isset($_GET["cantidad"])) {
    $cantidad = $_GET["cantidad"];
}
$pagina = 1;
if (isset($_GET["pagina"])) {
    $pagina = $_GET["pagina"];
}
$productos = $producto->consultarPaginacion($cantidad, $pagina);
$totalRegistros = $producto->consultarCantidad();
$totalPaginas = intval($totalRegistros / $cantidad);
if ($totalRegistros % $cantidad != 0) {
    $totalPaginas++;
}
$ultimaPagina = ($totalPaginas == $pagina);
if (isset($_POST['btnAccion'])) {

    $var = new  DateTime();
    
    if($_SESSION['carrito'] ==""){
        $_SESSION['carrito'] = $var->getTimestamp();
    }
    
    $e = new Factura("",$_SESSION['carrito']);
    $e->insertar();
    $i =new ProductoF("",$_SESSION['carrito'],$_POST['nombre']);

    $a = new ProductoF(
        $_POST['id'],
        $_SESSION['carrito'], 
        $_POST['nombre'],
        $_POST['cant'],
        $_POST['precio']
    );
    $band= false;
    if($i ->consultarCantidad2() == 0 ){
        $band= true;
    $a->insertar();
    }
}
?>
<head>
<link rel="stylesheet" type="text/css" href="librerias/alertifyjs/css/alertify.css">
	<link rel="stylesheet" type="text/css" href="librerias/alertifyjs/css/themes/default.css">
	<script src="librerias/alertifyjs/alertify.js"></script>
	<link rel="stylesheet" type="text/css" href="librerias/select2/css/select2.css">
</head>
<body style="background-image: url(img/fondoJuice.jpg);
background-size: cover">
<div class="container mt-3">
    <?php if (isset($_POST['btnAccion'])) { 
        if($band == false ){?>
            <script>
                alertify.error("La pulpa ya se agregó!");
            </script>
        
        <?php }else{
    ?>  
        
            
        <div class="alert alert-success">
            <a href="index.php?pid= <?php echo base64_encode("mostrarCarrito.php") ?>" class="badge badge-success"><h5><i class="fas fa-shopping-cart"></i>  Ver carrito</h5></a>
        </div>
    <?php } }?>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header text-white bg-dark text-center">
                    <h4><i class="fas fa-shopping-basket"></i> Nuestros productos</h4>
                </div>
                <div class="container text-left lead">Resultados <?php echo (($pagina - 1) * $cantidad + 1) ?> al <?php echo (($pagina - 1) * $cantidad) + count($productos) ?> de <?php echo $totalRegistros ?> pulpas registradas</div>
                <div class="card-body">
                    <div class="row">
                        <?php foreach ($productos as $productoActual) { ?>

                            <div class="col-3">

                                <div class="card">
                                    <img title="<?php echo $productoActual->getNombre(); ?>" alt="<?php echo $productoActual->getNombre(); ?>" class="card-img-top" src="<?php echo $productoActual->getfoto(); ?>" data-toggle="gggg" data-trigger="hover" data-content="<?php echo $productoActual->getDescripcion() ?>" height="200px">

                                    <div class="card-body lead">
                                        <span><?php echo $productoActual->getNombre(); ?></span>
                                        <h5 class="card-title">$<?php echo $productoActual->getPrecio(); ?></h5>
                                        <form action="" method="post">

                                            <input type="hidden" name="id" id="id" value="<?php echo $productoActual->getIdProducto(); ?>">
                                            <input type="hidden" name="nombre" id="nombre" value="<?php echo $productoActual->getNombre(); ?>">
                                            <input type="hidden" name="precio" id="precio" value="<?php echo $productoActual->getPrecio(); ?>">
                                            <input type="number" class="form-control" name="cant"  min ="1" max="<?php echo $productoActual->getCantidad();  ?>" value="1">
                                            <br>
                                            <button class="btn btn-success " name="btnAccion" value="Agregar" type="submit">
                                                <h2 class="lead">Agregar al carrito</h2>
                                            </button>
                                        </form>

                                    </div>
                                </div>

                            </div>

                        <?php } ?>

                    </div>
                    <div class="text-center">
                        <nav>
                            <ul class="pagination">
                                <li class="page-item <?php echo ($pagina == 1) ? "disabled" : ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("Presentacion/producto/consultarProductoPagina.php") . "&pagina=" . ($pagina - 1) . "&cantidad=" . $cantidad ?>"> &lt;&lt; </a></li>
                                <?php
                                for ($i = 1; $i <= $totalPaginas; $i++) {
                                    if ($i == $pagina) {
                                        echo "<li class='page-item active' aria-current='page'><span class='page-link'>" . $i . "<span class='sr-only'></span></span></li>";
                                    } else {
                                        echo "<li class='page-item'><a class='page-link' href='index.php?pid=" . base64_encode("Presentacion/producto/consultarProductoPagina.php") . "&pagina=" . $i . "&cantidad=" . $cantidad . "'>" . $i . "</a></li>";
                                    }
                                }
                                ?>
                                <li class="page-item <?php echo ($ultimaPagina) ? "disabled" : ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("Presentacion/producto/consultarProductoPagina.php") . "&pagina=" . ($pagina + 1) . "&cantidad=" . $cantidad ?>"> &gt;&gt; </a></li>
                            </ul>
                        </nav>
                    </div>
                    <select id="cantidad">
                        <option value="8" <?php echo ($cantidad == 8) ? "selected" : "" ?>>8</option>
                        <option value="16" <?php echo ($cantidad == 16) ? "selected" : "" ?>>16</option>
                        <option value="24" <?php echo ($cantidad == 24) ? "selected" : "" ?>>24</option>
                        <option value="32" <?php echo ($cantidad == 32) ? "selected" : "" ?>>32</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>
    
</body>
<script>
    $(function() {
        $('[data-toggle="gggg"]').popover()
    });
    $("#cantidad").on("change", function() {
        url = "index.php?pid=<?php echo base64_encode("Presentacion/producto/consultarProductoPagina.php") ?>&cantidad=" + $(this).val();
        location.replace(url);
    });
</script>