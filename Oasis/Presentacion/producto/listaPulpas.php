<?php

$producto = new Producto();
$cantidad = 8;
if (isset($_GET["cantidad"])) {
    $cantidad = $_GET["cantidad"];
}
$pagina = 1;
if (isset($_GET["pagina"])) {
    $pagina = $_GET["pagina"];
}
$productos = $producto->consultarPaginacion($cantidad, $pagina);
$totalRegistros = $producto->consultarCantidad();
$totalPaginas = intval($totalRegistros / $cantidad);
if ($totalRegistros % $cantidad != 0) {
    $totalPaginas++;
}
$ultimaPagina = ($totalPaginas == $pagina);

?>
<body style="background-image: url(img/fondoDurazno.jpg);
 background-size:cover;">
    
</body>
<div class="container mt-3">

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header text-white bg-dark text-center">
                    <h2><i class="fas fa-shopping-basket"></i> Nuestros productos</h2>
                </div>
                <div class="container text-left lead">Resultados <?php echo (($pagina - 1) * $cantidad + 1) ?> al <?php echo (($pagina - 1) * $cantidad) + count($productos) ?> de <?php echo $totalRegistros ?> productos registrados</div>
                <div class="card-body">
                    <div class="row">
                        <?php foreach ($productos as $productoActual) { ?>

                            <div class="col-3">

                                <div class="card">
                                    <img title="<?php echo $productoActual->getNombre(); ?>" alt="<?php echo $productoActual->getNombre(); ?>" class="card-img-top" src="<?php echo $productoActual->getfoto(); ?>" data-toggle="gggg" data-trigger="hover" data-content="<?php echo $productoActual->getDescripcion() ?>" height="200px">

                                    <div class="card-body ">
                                        <span><?php echo $productoActual->getNombre(); ?></span>
                                        <h5 class="card-title">$<?php echo $productoActual->getPrecio(); ?></h5>
                                        <h5><?php echo $productoActual->getCantidad(); ?> unidades</h5>
                                        <form action="" method="post">

                                            <input type="hidden" name="id" id="id" value="<?php echo $productoActual->getIdProducto(); ?>">
                                            <input type="hidden" name="nombre" id="nombre" value="<?php echo $productoActual->getNombre(); ?>">
                                            <input type="hidden" name="precio" id="precio" value="<?php echo $productoActual->getPrecio(); ?>">
                                            <input type="hidden" name="cantidad" id="cantidad" value="<?php echo $productoActual->getCantidad();?>">

                                            <a href="index.php?pid= <?php echo base64_encode("Presentacion/producto/crearProducto.php") ?>" class="btn btn-primary">
                                                Registrar otra pulpa
                                            </a>

                                        </form>

                                    </div>
                                </div>

                            </div>

                        <?php } ?>

                    </div>
                    <div class="text-center">
                        <nav>
                            <ul class="pagination">
                                <li class="page-item <?php echo ($pagina == 1) ? "disabled" : ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("Presentacion/producto/listaPulpas.php") . "&pagina=" . ($pagina - 1) . "&cantidad=" . $cantidad ?>"> &lt;&lt; </a></li>
                                <?php
                                for ($i = 1; $i <= $totalPaginas; $i++) {
                                    if ($i == $pagina) {
                                        echo "<li class='page-item active' aria-current='page'><span class='page-link'>" . $i . "<span class='sr-only'></span></span></li>";
                                    } else {
                                        echo "<li class='page-item'><a class='page-link' href='index.php?pid=" . base64_encode("Presentacion/producto/listaPulpas.php") . "&pagina=" . $i . "&cantidad=" . $cantidad . "'>" . $i . "</a></li>";
                                    }
                                }
                                ?>
                                <li class="page-item <?php echo ($ultimaPagina) ? "disabled" : ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("Presentacion/producto/listaPulpas.php") . "&pagina=" . ($pagina + 1) . "&cantidad=" . $cantidad ?>"> &gt;&gt; </a></li>
                            </ul>
                        </nav>
                    </div>
                 
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function() {
        $('[data-toggle="gggg"]').popover()
    });
    $("#cantidad").on("change", function() {
        url = "index.php?pid=<?php echo base64_encode("presentacion/producto/consultarProductoPagina.php") ?>&cantidad=" + $(this).val();
        location.replace(url);
    });
</script>