<?php
$producto = new Producto();
$productos = $producto -> consultarTodos();
?>
<body style="background-image: url(img/fondoMFrutas.jpg);
 background-repeat: repeat-y;">

	 
 </body>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card mt-5 ">
				<div class="card-header text-white bg-dark text-center">
					<h2><i class="fas fa-leaf"></i> Consultar Pulpas</h2>
				</div>
				<div class="text-left container lead"><?php echo count($productos) ?> registros encontrados</div>
              	<div class="card-body lead">
					<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Cantidad</th>
							<th>Precio</th>
							<th>Editar</th>
							<th></th>
						</tr>
						<?php 
						$i=1;
						foreach($productos as $productoActual){
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $productoActual -> getNombre() . "</td>";
						    echo "<td>" . $productoActual -> getCantidad() . "</td>";
							echo "<td>" . $productoActual -> getPrecio() . "</td>";
						    echo "<td><a  href='index.php?pid=". base64_encode("Presentacion/producto/editarProducto.php") . "&idProducto=" . $productoActual -> getIdProducto(). "' data-toggle='tooltip' data-placement='left' title='Editar'><span class='fas fa-pencil-alt text-primary'></span></a></td>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
				</div>
            </div>
		</div>
	</div>
</div>
<div>
	<br><br>
</div>