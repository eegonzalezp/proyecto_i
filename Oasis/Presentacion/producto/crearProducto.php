<?php
$nombre = "";
if (isset($_POST["nombre"])) {
	$nombre = $_POST["nombre"];
}
$cantidad = "";
if (isset($_POST["cantidad"])) {
	$cantidad = $_POST["cantidad"];
}
$precio = "";
if (isset($_POST["precio"])) {
	$precio = $_POST["precio"];
}

$img_nomb = "predeterminada.jpg";
$src = "";
$tiempo = new DateTime();
$destino = "img/siempre/";
if (isset($_POST["crear"])) {
	if($_FILES["foto"]["name"] != ""){
		$auxfoto = $_FILES["foto"]["tmp_name"];
		$type_foto = $_FILES["foto"]["type"];
		$img_nomb ="img_".$nombre.$tiempo -> getTimestamp().(($type_foto == "img/png")?".png":".jpg");
		$src =$destino.$img_nomb;
		copy($auxfoto,$src);
	}else{
		$src =$destino.$img_nomb;
	}

	$producto = new Producto("", $nombre, $cantidad, $precio,"", $src);
	$producto->insertar();

	$administrador = new Administrador($_SESSION['id']);
    $administrador -> consultar();

    $Log = new Log ("", "Registró una pulpa" ,$administrador->getNombre()." " . $administrador->getApellido(),"NOW()","NOW()","Administrador");
    $Log -> insertarLog() ;
}
?>
<html>

<head>
<link rel="stylesheet" type="text/css" href="librerias/alertifyjs/css/alertify.css">
	<link rel="stylesheet" type="text/css" href="librerias/alertifyjs/css/themes/default.css">
	<script src="librerias/alertifyjs/alertify.js"></script>
	<link rel="stylesheet" type="text/css" href="librerias/select2/css/select2.css">
</head>

<body style="background-image: url(img/fondoUva.jpg);
background-size: cover">
	
</body>
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-0 "></div>
			<div class="col-lg-6 col-md-12 mt-5">
				<div class="card">
					<div class="card-header text-white bg-dark text-center ">
						<h2><i class="fas fa-plus-circle"></i> Registrar Pulpa</h2>
					</div>
					<div class="card-body">

						<form action="index.php?pid=<?php echo base64_encode("Presentacion/producto/crearProducto.php") ?>" method="post" enctype="multipart/form-data">
							<div class="form-group">
								<label>Nombre</label>
								<select class="form-control" name="nombre" value="<?php echo $nombre ?>" required>>
									<option>Pulpa de mora</option>
									<option>Pulpa de uva</option>
									<option>Pulpa de fresa</option>
									<option>Pulpa de maracuya</option>
									<option>Pulpa de mango</option>
									<option>Pulpa de lulo</option>
									<option>Pulpa de guanabana</option>
									<option>Pulpa de guayaba</option>
									<option>Pulpa de frutos rojos</option>
								</select>
							</div>
							<div class="form-group">
								<label>Cantidad</label>
								<input type="number" name="cantidad" class="form-control" min="1" value="<?php echo $cantidad ?>" required>
							</div>
							<div class="form-group">
								<label>Precio</label>
								<input type="number" name="precio" class="form-control" min="1" value="<?php echo $precio ?>" required>
							</div>
							<div class="form-group">
								<label>Foto</label>
								<input type="file" name="foto" id="foto" >
							</div>
							<button type="submit" name="crear" class="btn btn-success btn-block"><i class="fas fa-check-circle"></i> Registrar pulpa</button>
						</form>
					</div>

					<?php if (isset($_POST["crear"])) { ?>
						<script>
							alertify.success("Pulpa registrada");
						</script>
					<?php } ?>

				</div>
			</div>
		</div>
	</div>
	<div>
		<br><br>
	</div>

</html>