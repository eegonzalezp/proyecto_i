<?php
$conexion = mysqli_connect('localhost', 'root', '', 'bdoasis');
?>

<head>

	<link href="Style/design.css" rel="stylesheet">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"> <!-- link de bootstrap -->
	<script src="js/funciones.js"></script>

</head>

<body style="background-image: url(img/fondoUvaList.jpg);
 background-repeat: repeat-y;">

	 
 </body>
	


	<div class="container">
		<div class="row">
			
			<div class="col-sm-12 mt-5">
				<div class="card">
					<div class="card-header text-white bg-dark text-center">
						<h2 class="text-center"><i class="fas fa-clipboard-list"></i> Actividad de usuarios</h2>
					</div>
					<div class="card-body">
						
						<table class="table table-hover table-condensed table-bordered table-striped" id="tablaLog" name="tablaLog">

							<tr class="bg-warning blockquote text-center">
								<td>#</td>
								<td>Acción</td>
								<td>Datos</td>
								<td>Fecha</td>
								<td>Hora</td>
								<td>Actor</td>

							</tr>

							<?php
							$sql = "SELECT id, accion, datos, fecha, hora, actor from logAdministrador";
							$result = mysqli_query($conexion, $sql);

							while ($mostrar = mysqli_fetch_array($result)) {

								$datos = $mostrar['id'] . "||" .
									$mostrar['accion'] . "||" .
									$mostrar['datos'] . "||" .
									$mostrar['fecha'] . "||" .
									$mostrar['hora'] . "||" .
									$mostrar['actor']
							?>
								<td class="text-center"><?php echo $mostrar['id'] ?></td>
								<td class="text-center"><?php echo $mostrar['accion'] ?></td>
								<td class="text-center"><?php echo $mostrar['datos'] ?></td>
								<td class="text-center"><?php echo $mostrar['fecha'] ?></td>
								<td class="text-center"><?php echo $mostrar['hora'] ?></td>
								<td class="text-center"><?php echo $mostrar['actor'] ?></td>


								</tr>
							<?php
							}
							?>
						</table>
					</div>
				</div>


			</div>
		</div>
		<div>
			<br><br>
		</div>
	</div>
	