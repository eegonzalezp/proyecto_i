
<?php
include "Presentacion/navegacion.php";
?>
<link href="Style/design.css" rel="stylesheet">

<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">

<head>
    <link rel="stylesheet" type="text/css" href="librerias/alertifyjs/css/alertify.css">
    <link rel="stylesheet" type="text/css" href="librerias/alertifyjs/css/themes/default.css">
    <script src="librerias/alertifyjs/alertify.js"></script>
    <link rel="stylesheet" type="text/css" href="librerias/select2/css/select2.css">

    <!--JQUERY-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Nuestro css-->
    <link rel="stylesheet" type="text/css" href="css/index.css" th:href="@{/css/index.css}">

</head>

<body>
    

    <div class="modal-dialog text-center mt-2">
        <div class="col-sm-8 main-section">
            <div class="modal-content ">
                <div class="col-12 user-img ">
                    <img src="img/user.png" />
                </div>
                <div class="card-body">
                    <!-- para llamar autenticacion toca desde la carpeta -->
                    <form action="index.php?pid=<?php echo base64_encode("Presentacion/autenticar.php") ?>" method="post">
                        <!-- para proteger la pagina -->
                        <div class="form-group">
                            <!-- para autenticar con id -->
                            <input name="correo" type="text" class="form-control" placeholder="Correo" required>
                        </div>
                        <div class="form-group">
                            <input name="clave" type="password" class="form-control" placeholder="Clave" required>
                        </div>
                        <div class="form-group">
                            <input name="ingresar" type="submit" class="form-control btn btn-primary text-left" value="Iniciar sesión">
                        </div>
                        <?php
                        if (isset($_GET["error"]) && $_GET["error"] == 1) { ?>
                            <script>
                                alertify.error("Correo o contraseña inconrrectos");
                            </script>

                        <?php
                        } else if (isset($_GET["error"]) && $_GET["error"] == 2) {
                        ?>
                            <script>
                                alertify.error("Su cuenta no ha sido activada");
                            </script>
                        <?php
                        } else if (isset($_GET["error"]) && $_GET["error"] == 3) {
                        ?>
                            <script>
                                alertify.error("Su cuenta se encuentra inactiva");
                            </script>
                        <?php
                        }
                        ?>


                    </form>
                    <p class="text-white">Eres nuevo? <a href="index.php?pid=<?php echo base64_encode("Presentacion/cliente/registrarCliente.php") ?>">Registrate</a></p>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <div>
        <br><br>
    </div>
</body>

</html>